using System;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Mvc;

namespace Blogs.Controllers {
    [Route ("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class UploadController : Controller {

        [HttpPost, DisableRequestSizeLimit]
        public IActionResult Upload () {
            try {
                var file = Request.Form.Files[0];

                if (file.Length > 0) {
                    var folderName = Path.Combine ("Resources", "Images");
                    var pathToSave = Path.Combine (Directory.GetCurrentDirectory (), folderName);
                    var fileName = ContentDispositionHeaderValue.Parse (file.ContentDisposition).FileName.Trim ('"');
                    var fullPath = Path.Combine (pathToSave, fileName);
                    var dbPath = Path.Combine (folderName, fileName);

                    using (var stream = new FileStream (fullPath, FileMode.Create)) {
                        file.CopyTo (stream);
                    }
                    return Ok (new { dbPath });
                } else {
                    return BadRequest ();
                }
            } catch (System.Exception ex) {
                return StatusCode (500, $"Internal server error: {ex}");
            }
        }
    }
}