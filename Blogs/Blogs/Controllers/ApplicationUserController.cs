﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blogs.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Blogs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class ApplicationUserController : ControllerBase
    {
        public UserManager<IdentityUser> userManager;
        public SignInManager<IdentityUser> signInManager;

        public ApplicationUserController(UserManager<IdentityUser> _userManager, SignInManager<IdentityUser> _signInManager)
        {
            userManager = _userManager;
            signInManager = _signInManager;
        }

        [HttpPost]
        [Route("Register")]
        public async Task<object> Register(ApplicationUserModel model)
        {
            var user = new IdentityUser()
            {
                UserName = model.UserName,
                Email = model.Email
            };

            var result = await userManager.CreateAsync(user, model.Password);
            return Ok(result);
        }

        [HttpPost]
        [Route("Login")]
        public async Task<object> Login(ApplicationUserModel model)
        {
            var result = await signInManager.PasswordSignInAsync(model.Email,
                          model.Password, true, lockoutOnFailure: true);
            return Ok(result);
        }

        [HttpPost]
        [Route("Logout")]
        public async Task<IActionResult> OnPost()
        {
            await signInManager.SignOutAsync();
            return Ok("Ok");
        }
    }
}