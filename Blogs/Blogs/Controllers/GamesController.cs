﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Blogs.Data;
using Blogs.Models;
using Microsoft.EntityFrameworkCore.Internal;

namespace Blogs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class GamesController : Controller
    {
        private readonly GamesContext _context;

        public GamesController(GamesContext context)
        {
            _context = context;
        }

        // GET: api/Games
        [HttpGet]
        public ActionResult GetGames()
        {
            return Ok(StatusCode(200, _context.Games.ToList()));
        }

        // GET: api/Games/5
        [HttpGet("{id}")]
        public async Task<ActionResult<object>> GetGames(int id)
        {
            var games = await _context.Games.FindAsync(id);

            if (games == null)
            {
                return NotFound();
            }

            return games;
        }

        // PUT: api/Games/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGames(int id, Games games)
        {
            if (id != games.GameId)
            {
                return BadRequest();
            }

            _context.Entry(games).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GamesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Games
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Games>> PostGames(Games games)
        {
            //_context.Games.Add(games);
            //await _context.SaveChangesAsync();
            if (ModelState.IsValid)
            {
                bool result = await _context.Create(games);

                return result == true ? CreatedAtAction("GetGames", new { id = games.GameId }, games) : null;
            }
            else
            {
                return BadRequest("Invalid data.");
            }
            
        }

        // DELETE: api/Games/5
        [HttpDelete("{id}")]
        public ActionResult DeleteGames(int? id)
        {
            if(id == null)
            {
                return BadRequest("Invalid data.");
            }

            var games = _context.GetById(id);
            if (games == null)
            {
                return NotFound();
            }

            //_context.Games.Remove(games);
            _context.Entry(games).State = EntityState.Deleted;
            _context.SaveChanges();

            return Ok();
        }

        private bool GamesExists(int id)
        {
            return _context.Games.Any(e => e.GameId == id);
        }
    }
}
