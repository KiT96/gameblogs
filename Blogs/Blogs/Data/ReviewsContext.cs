﻿using Blogs.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blogs.Data
{
    public class ReviewsContext : DbContext
    {
        public DbSet<Reviews> Reviews { get; set; }
        public IConfiguration Configuration { get; }

        public ReviewsContext(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            optionsBuilder.UseLoggerFactory(GetLoggerFactory());
        }

        private ILoggerFactory GetLoggerFactory()
        {
            IServiceCollection serviceCollection = new ServiceCollection();
            serviceCollection.AddLogging(builder =>
                    builder.AddConsole()
                           .AddFilter(DbLoggerCategory.Database.Command.Name,
                                    LogLevel.Information));
            return serviceCollection.BuildServiceProvider()
                    .GetService<ILoggerFactory>();
        }

        // Get all reviews game from database
        public async Task<List<Reviews>> GetAll()
        {
            try
            {
                return await Reviews.ToListAsync();
            }
            catch (Exception)
            {
                return null;
            }
        }

        // Get reviews by id from database
        public async Task<Reviews> GetById(int Id)
        {
            try
            {
                return await Reviews.Where(review => review.ReviewId == Id).FirstOrDefaultAsync();
            }
            catch (Exception)
            {
                return null;
            }
        }

        // Create a new review game
        public async Task<bool> Create(Reviews reviews)
        {
            try
            {
                await Reviews.AddAsync(new Reviews
                {
                    Title = reviews.Title,
                    Content = reviews.Content,
                    Rating = reviews.Rating,
                    GameId = reviews.GameId
                });
                int row = await SaveChangesAsync();
                return row > 0 ? true : false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        // Update a review game
        public async Task<bool> Update(Reviews reviews)
        {
            try
            {
                Reviews data = await Reviews.Where(review => review.ReviewId == reviews.ReviewId).FirstOrDefaultAsync();
                if(data != null)
                {
                    Reviews.Update(reviews);
                    int row = await SaveChangesAsync();
                    return row > 0 ? true : false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        // Delete a review game
        public async Task<bool> Delete(int Id)
        {
            try
            {
                Reviews data = await Reviews.Where(review => review.ReviewId == Id).FirstOrDefaultAsync();
                if (data != null)
                {
                    Reviews.Remove(data);
                    int row = await SaveChangesAsync();
                    return row > 0 ? true : false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
