﻿using Blogs.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blogs.Data
{
    public class GameTypeContext : DbContext
    {
        public DbSet<GameType> GameTypes { get; set; }
        public IConfiguration Configuration { get; }

        public GameTypeContext(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            optionsBuilder.UseLoggerFactory(GetLoggerFactory());
        }

        private ILoggerFactory GetLoggerFactory()
        {
            IServiceCollection serviceCollection = new ServiceCollection();
            serviceCollection.AddLogging(builder =>
                    builder.AddConsole()
                           .AddFilter(DbLoggerCategory.Database.Command.Name,
                                    LogLevel.Information));
            return serviceCollection.BuildServiceProvider()
                    .GetService<ILoggerFactory>();
        }

        // get all game type from database
        public async Task<List<GameType>> GetAll()
        {
            try
            {
                return await GameTypes.ToListAsync();
            }catch(Exception)
            {
                return null;
            }
        }

        // get game type by id from database
        public async Task<GameType> GetById(int Id)
        {
            try
            {
                return await GameTypes.Where(type => type.TypeId == Id).FirstOrDefaultAsync();
            }
            catch (Exception)
            {
                return null;
            }
        }

        // create new a game type
        public async Task<bool> Create(GameType type)
        {
            try
            {
                await GameTypes.AddAsync(new GameType
                {
                    Name = type.Name
                });
                int row = await SaveChangesAsync();
                return row > 0 ? true : false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        // update a game type
        public async Task<bool> Update(GameType type)
        {
            try
            {
                GameType data = await GameTypes.Where(x => x.TypeId == type.TypeId).FirstOrDefaultAsync();
                if(data != null)
                {
                    GameTypes.Update(type);
                    int row = await SaveChangesAsync();
                    return row > 0 ? true : false;
                }
                else
                {
                    return await Create(type);
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        // delete a game type
        public async Task<bool> Delete(int Id)
        {
            try
            {
                GameType data = await GameTypes.Where(type => type.TypeId == Id).FirstOrDefaultAsync();
                if(data != null)
                {
                    GameTypes.Remove(data);
                    int row = await SaveChangesAsync();
                    return row > 0 ? true : false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
