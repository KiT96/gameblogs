﻿using Blogs.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blogs.Data
{
    public class GamesContext : DbContext
    {
        public DbSet<Games> Games { get; set; }
        public IConfiguration Configuration { get; }

        public GamesContext(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            optionsBuilder.UseLoggerFactory(GetLoggerFactory());
        }

        private ILoggerFactory GetLoggerFactory()
        {
            IServiceCollection serviceCollection = new ServiceCollection();
            serviceCollection.AddLogging(builder =>
                    builder.AddConsole()
                           .AddFilter(DbLoggerCategory.Database.Command.Name,
                                    LogLevel.Information));
            return serviceCollection.BuildServiceProvider()
                    .GetService<ILoggerFactory>();
        }

        // Get all game from database
        public async Task<List<Games>> GetAll()
        {
            try
            {

                return await Games.ToListAsync();
            }
            catch (Exception)
            {
                return null;
            }
        }

        // Get game by id from database
        public async Task<Games> GetById(int? Id)
        {
            try
            {
                return await Games.Where(game => game.GameId == Id).FirstOrDefaultAsync();
            }
            catch(Exception)
            {
                return null;
            }
        }

        // Create new a game
        public async Task<bool> Create(Games games)
        {
            try
            {
                await Games.AddAsync(new Games
                {
                    Name = games.Name,
                    Thumbnail = games.Thumbnail,
                    Describe = games.Describe,
                    ReleaseDate = DateTime.Parse(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")),
                    TypeId = games.TypeId
                });
                int row = await SaveChangesAsync();
                return row > 0 ? true : false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        // Update a game
        public async Task<bool> Update(Games games)
        {
            try
            {
                Games data = await Games.Where(game => game.GameId == games.GameId).FirstOrDefaultAsync();
                if(data != null)
                {
                    Games.Update(games);
                    int row = await SaveChangesAsync();
                    return row > 0 ? true : false;
                }
                else
                {
                    return await Create(games);
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        // Delete a gamee
        public async Task<bool> Delete(int Id)
        {
            try
            {
                Games data = await Games.Where(game => game.GameId == Id).FirstOrDefaultAsync();
                if(data != null)
                {
                    Games.Remove(data);
                    int row = await SaveChangesAsync();
                    return row > 0 ? true : false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
