﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blogs.Models
{
    [Table("Games")]
    public class Games
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GameId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Thumbnail { get; set; }

        [Required]
        public string Describe { get; set; }

        [Required]
        public DateTime ReleaseDate { get; set; }

        [Required]
        public int TypeId { get; set; }

        public ICollection<Reviews> Reviews { get; set; }
    }
}
