export class Type{
    id: number;
    name: string;

    constructor(_name: string){
        this.name = _name;
    }
}