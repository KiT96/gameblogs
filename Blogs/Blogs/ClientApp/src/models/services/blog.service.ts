import { Injectable, Inject } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Blog } from "../../models/Blog";

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  public http: HttpClient;
  public baseUrl: string = '';

  constructor(_http: HttpClient, @Inject("BASE_URL") _baseUrl: string) {
    this.http = _http;
    this.baseUrl = _baseUrl;
  }

  getAll() {
    return new Promise<Array<Blog>>((resolve, reject) => {
      this.http
        .get<Array<Blog>>(this.baseUrl + "api/blog/index")
        .subscribe(result => resolve(result), error => reject(error));
    })
  }

  getById(id: number) {
    return new Promise<Blog>((resolve, reject) => {
      this.http
        .get<Blog>(this.baseUrl + "api/blog/detail/" + id)
        .subscribe(result => resolve(result), error => reject(error));
    })
  }

  getByTitle(title: string){
    return new Promise<Array<Blog>>((resolve, reject) => {
      this.http
        .get<Array<Blog>>(this.baseUrl + "api/blog/searchread/" + title)
        .subscribe(result => resolve(result), error => reject(error));
    })
  }

  create(blog: Blog, success: any, failed: any){
    this.http
    .post(this.baseUrl + "api/blog/create", blog)
    .subscribe(() => success, () => failed);
  }
}
