import { Injectable, Inject } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Account } from './../Account';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  public http: HttpClient = null;
  public baseUrl: string = '';

  constructor(_http: HttpClient, @Inject("BASE_URL") _baseUrl: string) {
    this.http = _http;
    this.baseUrl = _baseUrl;
  }

  login(account: Account, success: any, failed: any) {
    this.http.post(this.baseUrl + 'api/account/login', account)
      .subscribe(res => success(res), err => failed(err))
  }

  register(account: Account, success: any, failed: any) {
    this.http.post(this.baseUrl + 'api/account/register', account)
      .subscribe(res => success(res), err => failed(err))
  }
}
