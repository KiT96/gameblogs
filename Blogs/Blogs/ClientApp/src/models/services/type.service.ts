import { Injectable, Inject } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Type } from "../Type";

@Injectable({
  providedIn: 'root'
})
export class TypeService {
  public http: HttpClient;
  public baseUrl: string = '';

  constructor(_http: HttpClient, @Inject("BASE_URL") _baseUrl: string) {
    this.http = _http;
    this.baseUrl = _baseUrl;
  }

  get_all = () => {
    return new Promise<Array<Type>>((resolve, reject) => {
      this.http.get<Array<Type>>(this.baseUrl + 'api/GameTypes').subscribe(success => resolve(success), fail => reject(fail))
    })
  }

  create = (type: Type, success: any, failed: any) => {
    this.http
        .post(this.baseUrl + "api/GameTypes", type)
        .subscribe(() => success, () => failed);
  }

  update = (id: number, type: Type, success: any, failed: any) => {
    this.http
        .put(this.baseUrl + "api/GameTypes/" + id, type)
        .subscribe(() => success, () => failed);
  }

  delete = (id: number, success: any, failed: any) => {
    this.http
        .delete(this.baseUrl + "api/GameTypes/" + id)
        .subscribe(() => success, () => failed);
  }
}
