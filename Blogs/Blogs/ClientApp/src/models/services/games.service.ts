import { Injectable, Inject } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Games } from "../Games";

@Injectable({
  providedIn: 'root'
})
export class GamesService {
  public http: HttpClient;
  public baseUrl: string = '';

  constructor(_http: HttpClient, @Inject("BASE_URL") _baseUrl: string) {
    this.http = _http;
    this.baseUrl = _baseUrl;
  }

  get_all = () => {
    return new Promise<Array<Games>>((resolve, reject) => {
      this.http.get<Array<Games>>(this.baseUrl + 'api/Games').subscribe(success => resolve(success), fail => reject(fail))
    })
  }

  get_by_id = (id: number) => {
    return new Promise<Games>((resolve, reject) => {
      this.http.get<Games>(this.baseUrl + 'api/Games/' + id).subscribe(success => resolve(success), fail => reject(fail))
    })
  }

  create = (game: Games, success: any, failed: any) => {
    this.http
      .post(this.baseUrl + "api/Games", game)
      .subscribe(() => success, () => failed);
  }

  update = (id: number, game: Games, success: any, failed: any) => {
    this.http
      .put(this.baseUrl + "api/Games/" + id, game)
      .subscribe(() => success, () => failed);
  }

  delete = (id: number, success: any, failed: any) => {
    this.http
      .delete(this.baseUrl + "api/Games/" + id)
      .subscribe(() => success, () => failed);
  }
}
