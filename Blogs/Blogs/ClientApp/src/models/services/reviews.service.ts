import { Injectable, Inject } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Reviews } from "../Reviews";

@Injectable({
  providedIn: 'root'
})
export class ReviewsService {
  public http: HttpClient;
  public baseUrl: string = '';

  constructor(_http: HttpClient, @Inject("BASE_URL") _baseUrl: string) {
    this.http = _http;
    this.baseUrl = _baseUrl;
  }

  get_all = () => {
    return new Promise<Array<object>>((resolve, reject) => {
      this.http.get<Array<object>>(this.baseUrl + 'api/Reviews').subscribe(success => resolve(success), fail => reject(fail))
    })
  }

  create = (review: Reviews, success: any, failed: any) => {
    this.http
        .post(this.baseUrl + "api/Reviews", review)
        .subscribe(() => success, () => failed);
  }

  update = (id: number, review: Reviews, success: any, failed: any) => {
    this.http
        .put(this.baseUrl + "api/Reviews/" + id, review)
        .subscribe(() => success, () => failed);
  }

  delete = (id: number, success: any, failed: any) => {
    this.http
        .delete(this.baseUrl + "api/Reviews/" + id)
        .subscribe(() => success, () => failed);
  }
}
