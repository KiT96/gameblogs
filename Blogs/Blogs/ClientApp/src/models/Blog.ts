export class Blog {
    id: number;
    title: string;
    thumbnail: string;
    content: string;
    writeDate: any;
    summary: string;

    constructor(_title: string,
        _thumbnail: string,
        _content: string,
        _summary: string, ) {
        this.title = _title;
        this.thumbnail = _thumbnail;
        this.content = _content;
        this.summary = _summary;
    }

}