export class Account {
    id: number;
    email: string;
    password: string;
    rememberme: boolean;

    constructor(_email: string, _password: string, _rememberme: boolean){
        this.email = _email;
        this.password = _password;
        this.rememberme = _rememberme;
    }
}