export class Reviews{
    id: number;
    title: string;
    content: string;
    rating: string;
    gameId: number;

    constructor(_id: number, _title: string, _content: string, _rating: string, _gameId: number){
        this.id=_id;
        this.title=_title;
        this.content=_content;
        this.rating=_rating;
        this.gameId=_gameId;
    }
}