export class Games{
    gameId: number;
    name: string;
    thumbnail: string;
    describe: string;
    release_date: any;
    type_id: number;
    reviews: any

    constructor(_name: string, _thumbnail: string, _describe: string, _type_id: number){
        this.name = _name;
        this.thumbnail = _thumbnail;
        this.describe = _describe;
        this.type_id = _type_id;
    }
}