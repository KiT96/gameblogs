import { BrowserModule, Title } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { NgxPaginationModule } from 'ngx-pagination';

import { AppComponent } from "./app.component";
import { HomeComponent } from "./home/home.component";
import { LoginComponent } from "./auth/login/login.component";
import { RegisterComponent } from "./auth/register/register.component";
import { NavMenuComponent } from "./nav-menu/nav-menu.component";
import { UploadFileComponent } from './upload-file/upload-file.component';
import { ErrorValidateComponent } from "./auth/error-validate/error-validate.component";
import { ContactComponent } from "./contact/contact.component";
import { GameComponent } from "./game-blogs/game/game.component";
import { NewGameComponent } from "./game-blogs/new-game/new-game.component";
import { ReviewGameComponent } from "./game-blogs/review-game/review-game.component";
import { GameDetailComponent } from "./game-blogs/game-detail/game-detail.component";
import { CreateGameComponent } from "./game-blogs/create-game/create-game.component";
import { ReviewDetailComponent } from "./game-blogs/review-detail/review-detail.component";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    NavMenuComponent,
    UploadFileComponent,
    ErrorValidateComponent,
    ContactComponent,
    GameComponent,
    NewGameComponent,
    ReviewGameComponent,
    GameDetailComponent,
    CreateGameComponent,
    ReviewDetailComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: "ng-cli-universal" }),
    HttpClientModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot([
      { path: "", component: HomeComponent, pathMatch: "full" },
      { path: "home/auth/login", component: LoginComponent },
      { path: "home/auth/register", component: RegisterComponent },
      { path: "home/contact", component: ContactComponent },
      { path: "home/games", component: GameComponent },
      { path: "home/games/news", component: NewGameComponent },
      { path: "home/games/reviews", component: ReviewGameComponent },
      { path: "home/games/detail/:id", component: GameDetailComponent },
      { path: "home/games/new", component: CreateGameComponent },
      { path: "home/games/detail/review/:id", component: ReviewDetailComponent }
    ])
  ],
  providers: [Title],
  bootstrap: [AppComponent]
})
export class AppModule {}
