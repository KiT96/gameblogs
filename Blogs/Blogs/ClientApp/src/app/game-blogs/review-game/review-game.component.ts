import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { Reviews } from "../../../models/Reviews";
import { ReviewsService } from "../../../models/services/reviews.service";
import { GamesService } from "../../../models/services/games.service";
import { Games } from 'src/models/Games';

@Component({
  selector: 'app-review-game',
  templateUrl: './review-game.component.html',
  styleUrls: ['./review-game.component.css']
})
export class ReviewGameComponent implements OnInit {
  public title: string = 'EndGam - Reviews';
  public reviewsService: ReviewsService = null;
  public gamesService: GamesService = null;
  public reviews: Array<object> = []
  public games: Array<Games> = [];
  public imageNotFound: string = '';
  public page: number = 1;
  public totalRecords: number = 0;

  constructor(private titleService: Title, _reviewsService: ReviewsService, _gameService: GamesService) { 
    this.reviewsService = _reviewsService;
    this.gamesService = _gameService;
    this.imageNotFound = this.reviewsService.baseUrl + 'Resources\\Images\\not_found.png';
  }

  async ngOnInit() {
    this.titleService.setTitle(this.title)
    this.reviews = await this.reviewsService.get_all();
  }
}
