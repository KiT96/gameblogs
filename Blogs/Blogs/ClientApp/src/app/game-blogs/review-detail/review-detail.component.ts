import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { Subscription } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import { GamesService } from 'src/models/services/games.service';
import { Games } from 'src/models/Games';

@Component({
  selector: 'app-review-detail',
  templateUrl: './review-detail.component.html',
  styleUrls: ['./review-detail.component.css']
})
export class ReviewDetailComponent implements OnInit, OnDestroy {
  public title: string = 'EndGam - Rating game';
  public games: Games = null;
  public gamesService: GamesService = null;
  public activatedRoute: ActivatedRoute;
  public subscription: Subscription;
  public price: number = 0;
  public graph: number = 0;
  public level: number = 0;
  public diff: number = 0;
  public rate: any;

  constructor(private titleService: Title, _gamesService: GamesService, _activatedRoute: ActivatedRoute) {
    this.gamesService = _gamesService;
    this.activatedRoute = _activatedRoute;
  }

  ngOnInit() {
    this.titleService.setTitle(this.title)
    this.handleParams();
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe()
    }
  }

  handleParams() {
    this.subscription = this.activatedRoute.params.subscribe(async data => {
      this.games = await this.gamesService.get_by_id(data.id);
      console.log('this.games: ', this.games);
    })
  }

  calAvgRate = (event: { target: { value: number; }; }) => {
    console.log('event: ', event);
    this.rate = ((this.price + this.graph + this.level + this.diff) / 4).toFixed(2)
  }

}
