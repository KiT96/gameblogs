import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { Games } from '../../../models/Games';
import { GamesService } from '../../../models/services/games.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  public title: string = 'EndGam - Games';
  public games: Array<Games> = []
  public gamesService: GamesService = null;
  public page: number = 1;

  public constructor(private titleService: Title, _gamesService: GamesService) { this.gamesService = _gamesService }

  async ngOnInit(): Promise<void> {
    this.titleService.setTitle(this.title)
    this.games = await this.gamesService.get_all();
  }

}
