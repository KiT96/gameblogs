import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { Type } from "../../../models/Type";
import { TypeService } from "../../../models/services/type.service";

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.css']
})
export class NewGameComponent implements OnInit {
  public title: string = 'EndGam - News';
  public types: Array<Type> = null;
  public typeService: TypeService = null;

  constructor(private titleService: Title, _typeService: TypeService) { 
    this.types = new Array<Type>();
    this.typeService = _typeService;
  }

  async ngOnInit() {
    this.titleService.setTitle(this.title)
    this.types = await this.typeService.get_all();
  }

}
