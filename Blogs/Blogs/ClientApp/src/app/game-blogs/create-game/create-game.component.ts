import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { Router } from "@angular/router";
import { Type } from './../../../models/Type';
import { TypeService } from './../../../models/services/type.service';
import { Games } from 'src/models/Games';
import { GamesService } from './../../../models/services/games.service';

@Component({
  selector: 'app-create-game',
  templateUrl: './create-game.component.html',
  styleUrls: ['./create-game.component.css']
})
export class CreateGameComponent implements OnInit {
  public title: string = 'EndGam - Create new a game';
  public types: Array<Type> = [];
  public typeService: TypeService = null;
  public gamesService: GamesService = null;
  public router: Router;
  public optionsSelect: Array<any> = [];

  public nameGame: string = '';
  public thumbnail: string = '';
  public typeId: number;
  public description: string = '';
  public message: string = '';

  constructor(private titleService: Title, _typeService: TypeService, _gamesService: GamesService, _router: Router) { 
    this.typeService = _typeService; 
    this.gamesService = _gamesService;
    this.router = _router; 
  }

  async ngOnInit() {
    this.titleService.setTitle(this.title)
    this.types = await this.typeService.get_all();
    this.types.unshift(new Type('Choose the game category'))
  }

  public uploadFinished = (event: { dbPath: string; }) => {
    this.thumbnail = event.dbPath;
    console.log(event.dbPath);
  }

  resetData() {
    this.nameGame = '';
    this.thumbnail = '';
    this.typeId = 0;
    this.description = '';
    this.message = '';
  }

  async checkData(event: { target: { value: any; }; }){
    if(event.target.value !== ''){
      this.message = '';
    }
  }

  public onCreate() {
    if(this.nameGame.length == 0 || this.thumbnail.length == 0 || this.description.length == 0 || this.typeId == undefined){
      this.message = 'Please enter full information.';
    }else{
      this.message = ''
      var game = new Games(this.nameGame, this.thumbnail, this.description, this.typeId);
      this.gamesService.create(game, this.router.navigate(['/home/games']), this.resetData())
    }
    
  }

}
