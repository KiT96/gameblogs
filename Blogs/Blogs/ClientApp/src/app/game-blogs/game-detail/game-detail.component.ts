import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { Subscription } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import { GamesService } from 'src/models/services/games.service';
import { Games } from 'src/models/Games';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.css']
})
export class GameDetailComponent implements OnInit, OnDestroy {
  public title: string = 'EndGam - Details';
  public games: Games = null;
  public gamesService: GamesService = null;
  public activatedRoute: ActivatedRoute;
  public subscription: Subscription;

  constructor(private titleService: Title, _gamesService: GamesService, _activatedRoute: ActivatedRoute) {
    this.gamesService = _gamesService;
    this.activatedRoute = _activatedRoute;
  }

  ngOnInit() {
    this.titleService.setTitle(this.title)
    this.handleParams();
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe()
    }
  }

  handleParams() {
    this.subscription = this.activatedRoute.params.subscribe(async data => {
      this.games = await this.gamesService.get_by_id(data.id);
    })
  }

}
