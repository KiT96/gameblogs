import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { HttpClient, HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.css']
})
export class UploadFileComponent implements OnInit {
  public progress: number = 0;
  public message: string = '';
  @Output() public onUploadFinished = new EventEmitter();
  public http: HttpClient;
  public baseUrl: string = '';

  constructor(_http: HttpClient, @Inject("BASE_URL") _baseUrl: string) {
    this.http = _http;
    this.baseUrl = _baseUrl;
  }

  ngOnInit() {
  }

  /**
   * uploadFile = files  =>  */
  public uploadFile = (files: string | any[]) => {
    if (files.length === 0) {
      return;
    } else {
      let fileToUpload = <File>files[0];
      const formData = new FormData();
      formData.append('file', fileToUpload, fileToUpload.name);

      this.http.post(this.baseUrl + 'api/upload', formData, { reportProgress: true, observe: 'events' })
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) this.progress = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response) {
            this.message = 'Upload success.';
            this.onUploadFinished.emit(event.body);
          }
        })
    }
  }

}
