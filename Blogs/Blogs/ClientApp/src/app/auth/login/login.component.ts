import { Component, OnInit } from '@angular/core';
import { AccountService } from "../../../models/services/account.service";
import { Account } from './../../../models/Account';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Title } from "@angular/platform-browser";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public title: string = 'EndGam - Log into the website';
  public accountService: AccountService;
  public formUser: FormGroup;

  constructor(_accountService: AccountService, private _formBuider: FormBuilder, private titleService: Title) {
    this.accountService = _accountService;
  }

  ngOnInit(): void {
    this.titleService.setTitle(this.title)
    this.createForm();
  }

  createForm() {
    this.formUser = this._formBuider.group({
      email: ['', [
        Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'),
      ]],
      password: ['', [
        Validators.required, Validators.minLength(6), Validators.maxLength(12)
      ]],
      rememberme: [false]
    })

    this.formUser.valueChanges.subscribe(data => {
      console.log('formUser: ', this.formUser);
    })
  }

  onSubmit() {
    var value = this.formUser.value;
    var account = new Account(value.email, value.password, value.rememberme);
    console.log('account: ', account);
    this.accountService.login(account, () => console.log('Login success'), () => console.log('Login failed'));
  }

}
