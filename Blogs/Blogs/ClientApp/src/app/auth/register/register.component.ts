import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/models/services/account.service';
import { Account } from './../../../models/Account';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Title } from "@angular/platform-browser";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public title: string = 'EndGam - Register an account';
  public accountService: AccountService;
  public formUser: FormGroup;

  constructor(_accountService: AccountService, private _formBuider: FormBuilder, private titleService: Title) { this.accountService = _accountService }

  ngOnInit(): void {
    this.titleService.setTitle(this.title)
    this.createForm();
  }

  createForm() {
    this.formUser = this._formBuider.group({
      username: ['',[Validators.required, Validators.minLength(6), Validators.maxLength(30)]],
      email: ['',[
        Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'),
      ]],
      password: ['',[
        Validators.required, Validators.minLength(6), Validators.maxLength(12)
      ]],
      password_confirm: ['',[
        Validators.required, Validators.minLength(6), Validators.maxLength(12)
      ]],
    })

    this.formUser.valueChanges.subscribe(data => {
      console.log('formUser: ', this.formUser);
    })
  }

  onSubmit() {
    var value = this.formUser.value;
    var account = new Account(value.email, value.password, false);
    this.accountService.register(account, () => console.log('Register success'), () => console.log('Register failed'));
  }

}
